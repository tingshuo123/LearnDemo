package com.example.demo.confg;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动转 HTTPS 配置类
 * @author wangqingjie
 *
 * 2018年12月4日-上午11:19:04
 */
@Configuration
public class SLLAutoRedirectConfig {
	
	@Bean
	public Connector connector() {
		Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		connector.setScheme("http");
		connector.setPort(8080);  // 监听 8081 端口
		connector.setSecure(false);
		connector.setRedirectPort(8081);  // 重定向到 8080 端口
		
		return connector;
	}
	
	@Bean
	public TomcatServletWebServerFactory tomcatServletWebServerFactory(Connector connector) {
		TomcatServletWebServerFactory tomcatServletWebServerFactory = new TomcatServletWebServerFactory() {
			@Override
			protected void postProcessContext(Context context) {
				SecurityConstraint securityConstraint=new SecurityConstraint();
                securityConstraint.setUserConstraint("CONFIDENTIAL");
                SecurityCollection collection=new SecurityCollection();
                collection.addPattern("/*");
                securityConstraint.addCollection(collection);
                context.addConstraint(securityConstraint);
			};
		};
		tomcatServletWebServerFactory.addAdditionalTomcatConnectors(connector);
		
		return tomcatServletWebServerFactory;
	}
}
