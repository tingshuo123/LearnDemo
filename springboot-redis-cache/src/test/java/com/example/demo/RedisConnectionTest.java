package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisConnectionTest {
	
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	
	@Test
	public void test() {
		stringRedisTemplate.opsForValue().set("aaa", "1111");
		String value = stringRedisTemplate.opsForValue().get("aaa");
		TestCase.assertEquals("1111", value);
	}
}
