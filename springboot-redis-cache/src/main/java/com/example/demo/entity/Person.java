package com.example.demo.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity   // jpa 实体类注解
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"}) // 解决 No serializer 异常
public class Person implements Serializable{
	
	private static final long serialVersionUID = 460057158705609083L;

	@Id
	private Long id;
	
	private String name;
	
	private Integer age;
	
	private String address;
}
