package com.example.demo.mapper;


import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.Person;
import com.example.demo.entity.PersonExample;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonMapperTest {
    
    @Autowired
    PersonMapper mapper;
    
    @Test
    public void countByExample() {
        Long count = mapper.countByExample(null);
        System.out.println(count);
    }
    
    @Test
    public void deleteByExample() {
        PersonExample example = new PersonExample();
        example.createCriteria().andNameEqualTo("任平生");
        int a = mapper.deleteByExample(example);
    }
    
    @Test
    public void insert() {
        Person person = new Person();
        person.setId(1L);
        person.setName("任平生");
        person.setAge(17);
        person.setAddress("枫林小筑");
        mapper.insert(person);
    }
    
    @Test
    public void insertSelective() {
        Person person = new Person();
        person.setName("任平生");
        mapper.insertSelective(person);
    }
    
    @Test
    public void selectByExample() {
        PersonExample example = new PersonExample();
        example.createCriteria().andAgeBetween(17, 22);
        List<Person> list = mapper.selectByExample(example);
        list.forEach(System.out::println);
    }
    
    @Test
    public void updateByExampleSelective() {
        PersonExample example = new PersonExample();
        example.createCriteria().andAddressIsNull();
        
        Person person = new Person();
        person.setName("兰希");
        person.setAge(11);
        person.setAddress("瓦罗兰");
        
        mapper.updateByExampleSelective(person, example);
    }
}
