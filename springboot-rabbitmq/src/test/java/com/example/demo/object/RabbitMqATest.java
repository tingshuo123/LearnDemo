package com.example.demo.object;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.User;
import com.example.demo.object.ObjectSender;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitMqATest {
    
    @Autowired
    private ObjectSender sender;
    
    @Test
    public void test() {
        User user = new User();
        user.setName("tingshuo");
        user.setAge(20);
        sender.send(user);
    }
}
