package com.example.demo.topic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
/**
 * topic 模式测试
 * @author wqj24
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TopicTest {
    
   
    @Autowired
    TopicSender serder;
    
    @Test
    public void topicTest() {
        serder.send1();
        serder.send2();
    }
}
