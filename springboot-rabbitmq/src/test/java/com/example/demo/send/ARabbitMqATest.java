package com.example.demo.send;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.sender.ASender;
import com.example.demo.sender.BSender;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ARabbitMqATest {
    
    @Autowired
    private ASender aSender;
    
    @Autowired
    private BSender bSender;
    
    @Test
    public void hello() {
        for (int i=0; i < 10; i++) {
            // 多个消息发布者， 及多个消息订阅者，
//            aSender.send(i);
            bSender.send(i);
        }
    }
}
