package com.example.demo.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "topic.message")
public class TopicReceiver1 {
    
    @RabbitHandler
    public void process(String message) {
        
        // 处理消息
        System.out.println("Topic Receiver1: " + message);
    }
}
