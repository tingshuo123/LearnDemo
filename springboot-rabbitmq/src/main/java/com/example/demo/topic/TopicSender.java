package com.example.demo.topic;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TopicSender {
    
    @Autowired
    private AmqpTemplate  rabbitTemplate;
    
    public void send1() {
        String context = "消息 1";
        System.out.println("Sender :" + context);
        
        /*
         * 第一个参数：交换机（exchange）
         * 第二个参数：路由键（routing_key）
         * 第三个参数：消息（message）
         */
        this.rabbitTemplate.convertAndSend("exchange", "topic.message", context);
    }
    
    public void send2() {
        String context = "消息 2";
        System.out.println("Sender :" + context);
        this.rabbitTemplate.convertAndSend("exchange", "topic.messageA", context);
    }
}
