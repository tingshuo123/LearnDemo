package com.example.demo.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "A")  // 订阅 A 队列消息
public class AReceiver {
    
    @RabbitHandler
    public void process01(String msg) {
        // 处理消息
        System.out.println("Receiver B: " + msg);
    }
}
