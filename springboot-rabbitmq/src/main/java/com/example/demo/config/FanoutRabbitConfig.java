package com.example.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FanoutRabbitConfig {
    
    @Bean
    public Queue fanoutAQueue() {
        
        return new Queue("fanout.A");
    }
    @Bean
    public Queue fanoutBQueue() {
        
        return new Queue("fanout.B");
    }
    @Bean
    public Queue fanoutCQueue() {
        
        return new Queue("fanout.C");
    }
    
    /**
     * 注册一个 fanout 模式的交换机
     *
     */
    @Bean
    FanoutExchange fanoutExchange() {
        /*
         * 第一个参数交换机名字
         * 第二个参数是否持久化到硬盘（默认true）
         * 第三个参数没有绑定后是否自动删除（默认false）
         */
        FanoutExchange fanoutExchange = new FanoutExchange("fanoutExchange", false, false);
        
        return fanoutExchange;
    }
    
    // 将队列绑定到 fanout 模式交换机上
    @Bean
    Binding bindingExchangeA(Queue fanoutAQueue, FanoutExchange fanoutExchange) {
        
        return BindingBuilder.bind(fanoutAQueue).to(fanoutExchange);
    }
    
    @Bean
    Binding bindingExchangeB(Queue fanoutBQueue, FanoutExchange fanoutExchange) {
        
        return BindingBuilder.bind(fanoutBQueue).to(fanoutExchange);
    }
    
    @Bean
    Binding bindingExchangeC(Queue fanoutCQueue, FanoutExchange fanoutExchange) {
        
        return BindingBuilder.bind(fanoutCQueue).to(fanoutExchange);
    }
}
