package com.example.demo.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Rabbit 配置类（默认路由模式）
 *
 */
@Configuration
public class Rabbitconfig {
    
    /*
     * 注册队列
     */
    
    @Bean
    public Queue A() {
        // 队列名字
        return new Queue("A");
    }
    
    @Bean
    public Queue ObjectQueue() {
        // 队列名字
        return new Queue("objectQueue");
    }
}
