package com.example.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 交换机 topic 模式的配置
 *
 */
@Configuration
public class TopicRabbitConfig {
    
    /*
     * 在 topic 这中交换机模式下，
     * 路由键必须是一串字符，用句号（.） 隔开
     * 
     */
    
    final static String message = "topic.message";
    final static String messages = "topic.messages";
    
    // 注册两个队列
    @Bean
    public Queue queueMessage() {
        
        return new Queue(TopicRabbitConfig.message);
    }    
    @Bean
    public Queue queueMessages() {
        
        return new Queue(TopicRabbitConfig.messages);
    }
    
    // 注册一个交换机
    @Bean
    TopicExchange exchange() {
        
        return new TopicExchange("exchange");
    }
    
    // 
    @Bean
    Binding bindingExchangeMessage(Queue queueMessage, TopicExchange exchange) {
        
        // 只匹配 topic.message 路由键
        // 通过指定交换机，将只要是能根 topic.message 匹配的 routing_key, 发布到指定队列上
        return BindingBuilder.bind(queueMessage).to(exchange).with("topic.message");
    }
    
    @Bean
    Binding bindingExchangeMessages(Queue queueMessages, TopicExchange exchange) {
        
        // '#' 表示一个或多个单词 '*' 表示一个单词
        // 通过指定交换机，将只要是能跟 topic.# 匹配的 routing_key, 发布到指定队列上
        return BindingBuilder.bind(queueMessages).to(exchange).with("topic.#");
    }
}
