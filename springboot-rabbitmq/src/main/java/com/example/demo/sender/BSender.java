package com.example.demo.sender;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 发送者
 */
@Component
public class BSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send(int i) {
        System.out.println("Sender B: " + "msg " + i);
        this.rabbitTemplate.convertAndSend("A", "msg " + i);
    }
}
