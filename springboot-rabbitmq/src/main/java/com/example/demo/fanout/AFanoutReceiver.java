package com.example.demo.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "fanout.A")
public class AFanoutReceiver {
    
    @RabbitHandler
    public void process(String message) {
        
        // 处理消息
        System.out.println("Fanout ReceiverA: " + message);
    }
}
