package com.example.demo.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "fanout.B")
public class BFanoutReceiver {
    
    @RabbitHandler
    public void process(String message) {
        
        // 处理消息
        System.out.println("Fanout ReceiverB: " + message);
    }
}
