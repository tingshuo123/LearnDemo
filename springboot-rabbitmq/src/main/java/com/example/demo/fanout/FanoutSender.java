package com.example.demo.fanout;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FanoutSender {
    
    @Autowired
    private AmqpTemplate  rabbitTemplate;
    
    public void send() {
        
        String message = "广播消息：大家好";
        System.out.println("Sender: " + message);
        // fanout 模式下，routing_key 会被忽略，消息将会发送到跟 fanout 模式交换机绑定的所有队列上
        this.rabbitTemplate.convertAndSend("fanoutExchange", "a", message);
    }
}
