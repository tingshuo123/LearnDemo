package com.example.demo.object;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.example.demo.entity.User;

@Component

public class ObjectReceiver {
    
    // 处理 "objectQueue" 队列中的消息（对象）
    @RabbitListener(queues = "objectQueue")
    @RabbitHandler
    public void process01(User user) {
        // 处理消息
        System.out.println("ObjectReceiver: " + user);
    }
}
