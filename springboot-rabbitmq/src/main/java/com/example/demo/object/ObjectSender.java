package com.example.demo.object;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.entity.User;

/**
 * 发布者，发布对象
 * @author wqj24
 *
 */
@Component
public class ObjectSender {
    
    @Autowired
    private AmqpTemplate  rabbitTemplate;
    
    public void send(User user) {
        System.out.println("ObjectSender :" + user);
        // 发布对象到队列中
        this.rabbitTemplate.convertAndSend("objectQueue", user);
    }
}
