package com.projece.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.projece.demo.entity.UserEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
	
	@Autowired
	UserService service;
	
	@Test
	public void getTest() {
		UserEntity user1 = service.get1(1);
		UserEntity user2 = service.get2(1);
		System.out.println(user1);
		System.out.println(user2);
		
	}
}
