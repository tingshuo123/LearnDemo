package com.projece.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.projece.demo.mapper")
@SpringBootApplication
public class SpringbootDynamicDatasourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDynamicDatasourceApplication.class, args);
	}
}
