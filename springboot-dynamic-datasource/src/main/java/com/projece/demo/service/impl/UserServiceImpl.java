package com.projece.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.projece.demo.entity.UserEntity;
import com.projece.demo.mapper.UserMapper;
import com.projece.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	/*
	 * @DS 写在类上及方法上，也可以写在mapper接口上(不推介)，优先写在service方法上，其次是service实现类上
	 */
	@Autowired
	UserMapper userMapper;
	
	@Override
	public void addUser(UserEntity user) {
		
		userMapper.insert(user);
	}

	@Override
	@DS("slave_1")  // 切换成 slaver_1 数据源
	public UserEntity get1(int id) {
		
		return userMapper.getOne(id);
	}

	@Override
	@DS("master")
	public UserEntity get2(int id) {
		
		return userMapper.getOne(id);
	}
}
