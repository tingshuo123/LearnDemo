package com.projece.demo.service;

import com.projece.demo.entity.UserEntity;

public interface UserService {
	
	void addUser(UserEntity user);
	
	UserEntity get1(int id);
	
	UserEntity get2(int id);
}
