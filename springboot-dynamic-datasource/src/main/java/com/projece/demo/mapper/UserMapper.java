package com.projece.demo.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import com.projece.demo.entity.UserEntity;

public interface UserMapper {
	
	@Insert("insert into `user`(`name`, `age`) values(#{name}, #{age}")
	public void insert(UserEntity user);
	
	@Select("select id, name, age from user where id = #{id}")
	public UserEntity getOne(int id);
}
