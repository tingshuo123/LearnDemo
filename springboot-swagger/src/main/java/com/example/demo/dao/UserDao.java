package com.example.demo.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.example.demo.entity.User;

public interface UserDao extends BaseMapper<User>{
	
}
