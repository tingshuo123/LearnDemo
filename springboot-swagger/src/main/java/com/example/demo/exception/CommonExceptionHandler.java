package com.example.demo.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

/**
 * 同一异常处理类
 *
 */
@ControllerAdvice
@Slf4j
public class CommonExceptionHandler {
	
	@ExceptionHandler(CommonException.class)
	@ResponseBody
	public Map<String, Object> exeptionHandler(CommonException e) {
		log.info("CommonException:{}({})", e.getMsg(), e.getCode());
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("respCode", e.getCode());
		result.put("respMsg", e.getMsg());
		
		return result;
	}
}
