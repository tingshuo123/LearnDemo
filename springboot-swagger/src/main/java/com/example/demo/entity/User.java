package com.example.demo.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class User extends Model<User> {

	/**
	 * 序列化标识
	 */
	private static final long serialVersionUID = -9201776779954826738L;

	@TableId
	private Long id;

	private String code;

	private String name;

	private StatusEnum status;

	private Date gmtCreate;

	private Date gmtModified;

	
	public static final String ID = "id";

	public static final String CODE = "code";

	public static final String NAME = "name";

	public static final String GMT_CREATE = "gmt_create";

	public static final String GMT_MODIFIED = "gmt_modified";

	@Override
	protected Serializable pkVal() {

		return this.id;
	}

}
