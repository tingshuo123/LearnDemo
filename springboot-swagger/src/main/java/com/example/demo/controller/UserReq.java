package com.example.demo.controller;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//加入@ApiModel
@ApiModel
public class UserReq {
	
	@ApiModelProperty(value="ID", dataType="String", name="ID", example="1053194693111738369")
	private String id;
	
	@ApiModelProperty(value="编码", dataType="String", name="code", example="001")
	@NotBlank(message="名字不能为空")
	private String name;
	
	@ApiModelProperty(value="用户名", dataType="String", name="name", example="任平生")
	@NotBlank(message="编码不能为空")
	private String code;
}
