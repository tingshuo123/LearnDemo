package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.example.demo.entity.User;
import com.example.demo.exception.CommonException;
import com.example.demo.service.IUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 用户控制层
 * 简单展示增删查改及分页
 * swagger文档内容
 *
 */

@RestController
@RequestMapping("/user")
@Api(tags="UserController~用户API")
public class UserController {
	
	private static final UserResp UserResp = null;
	@Autowired
	IUserService userService;
	
	/**
	 * 新增用户
	 * @param userReq
	 * @return
	 */
	@GetMapping("/add")
	@ApiOperation(value = "新增用户")
	public Map<String, String> addUser(UserReq userReq) {
		
		User user = new User();
		user.setCode(userReq.getCode());
		user.setName(userReq.getName());
		// 已经设置了主键自动生成策略
		userService.insert(user);
		
		
		Map<String, String> result = new HashMap<String,String>();
		result.put("respCode", "01");
		result.put("respMsg", "新增成功");
		
		return result;
	}
	
	/**
	 * 更新用户
	 * @param userReq
	 * @return
	 * @throws CommonException
	 */
	@GetMapping("/update")
	@ApiOperation(value = "修改用户")
	public Map<String, String> updateUser(UserReq userReq) throws CommonException {
		
		if(userReq.getId() == null || "".equals(userReq.getId())) {
			throw new CommonException("0000", "更新时ID不能为空");
		}
		
		User user = new User();
		user.setCode(userReq.getCode());
		user.setName(userReq.getName());
		user.setId(Long.parseLong(userReq.getId()));
		userService.updateById(user);
		
		Map<String, String> result = new HashMap<>();
		result.put("respCode", "01");
		result.put("respMsg", "更新成功");
		
		return result;
	}
	
	/**
	 * 分页查询
	 * @param current
	 * @param size
	 * @return
	 */
	@GetMapping("/page")
	@ApiOperation(value = "查询用户（分页）")
	public Map<String, Object> pageUser(int current, int size) {
		
		Page<User> page = new Page<>(current, size);
		
		Map<String, Object> result = new HashMap<>();
		result.put("respCode", "01");
		result.put("respMsg", "查询成功");
		result.put("data", userService.selectPage(page));
		
		return result;
	}
	
	@GetMapping("/get/{id}")
	@ApiOperation(value = "查询用户（id)")
	public /*Map<String, Object>*/ UserResp getUser(@PathVariable("id") String id) throws CommonException {
		
		// 通过 id 查询用户信息
		User user = userService.selectById(id);
		if (user == null) {
			throw new CommonException("0001", "用户ID：" + id + ", 未找到");
		}
		
		UserResp resp = UserResp.builder()
				.id(user.getId().toString())
				.code(user.getCode())
				.name(user.getName())
				.status(user.getStatus())
				.build();
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("respCode", "01");
		result.put("respMsg", "成功");
		result.put("data", resp);
		
//		return result;
		return resp;
	}
}
