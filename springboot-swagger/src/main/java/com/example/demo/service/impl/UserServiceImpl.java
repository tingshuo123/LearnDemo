package com.example.demo.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.example.demo.dao.UserDao;
import com.example.demo.entity.User;
import com.example.demo.service.IUserService;

@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements IUserService{
	
	
}
