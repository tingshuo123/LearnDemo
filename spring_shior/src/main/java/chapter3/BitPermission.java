package chapter3;

import org.apache.shiro.authz.Permission;

import com.alibaba.druid.util.StringUtils;

/**
 * 自定义的权限匹配方法
 * 
 * 规则： +资源字符+权限位+实例
 * 
 * 以+开头，中间通过+分割
 * 
 * 权限： 0 表示所有权限 0000 1 添加 0001 2 修改 0010 4 删除 0100 8 查看 1000
 */
public class BitPermission implements Permission {

	// 资源标识
	private String resourceIdentify = null;
	// 权限标识
	private int permissionBit = 0;
	// 实例标识
	private String instanceId = null;

	public BitPermission(String permissionString) {

		// 以 + 为分割标记，分割字符串
		String[] array = permissionString.split("\\+");
		for (String string : array) {
			System.out.println(string);
		}
		// 获取资源标识
		if (array.length > 1) {
			this.resourceIdentify = array[1];
		}
		if (StringUtils.isEmpty(this.resourceIdentify)) {
			this.resourceIdentify = "*";
		}

		// 获取权限标识
		if (array.length > 2) {
			this.permissionBit = Integer.valueOf(array[2]);
		}

		// 获取用户标识
		if (array.length > 3) {
			instanceId = array[3];
		}
		if (StringUtils.isEmpty(instanceId)) {
			instanceId = "*";
		}
	}

	@Override
	public boolean implies(Permission p) {

		if (!(p instanceof BitPermission)) {
			return false;
		}

		BitPermission other = (BitPermission) p;

		if ("*".equals(this.resourceIdentify) || this.resourceIdentify.equals(other.resourceIdentify)) {
			return false;
		}

		if (!(this.permissionBit == 0) || ((this.permissionBit & other.permissionBit) == 0)) {
			return false;
		}

		if (!("*".equals(this.instanceId) || this.instanceId.equals(other.instanceId))) {
			return false;
		}

		return true;
	}

	@Override

	public String toString() {
		
		return "BitPermission{" +
				"resourceIdentify='" + resourceIdentify + '\'' +
				", permissionBit=" + permissionBit +
				", instanceId='" + instanceId + '\'' +
				'}';
	}
	
	public static void main(String[] args) {
		BitPermission permission = new BitPermission("+user+10");
		System.out.println(permission);
	}
}
