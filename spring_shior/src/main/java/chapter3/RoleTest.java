package chapter3;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;


public class RoleTest extends BaseTest{

	
	@Test
	public void testHasRole() {
		String configFile = "classpath:shiro-role.ini";
		String username = "zhang";
		String password = "123";
		
		this.loing(configFile, username, password);
		
		// 判断是否属于单个角色
		Assert.assertTrue(this.subject().hasRole("role1"));
		// 判断是否拥有多个角色
		Assert.assertTrue(this.subject().hasAllRoles(Arrays.asList("role1", "role2")));
	}
}
