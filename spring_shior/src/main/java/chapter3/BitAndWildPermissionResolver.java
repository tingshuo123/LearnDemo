package chapter3;

import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.PermissionResolver;
import org.apache.shiro.authz.permission.WildcardPermission;
/**
 * 自定义权限匹配
 * 根据权限字符串的特征，调用相应的Permission处理
 */
public class BitAndWildPermissionResolver implements PermissionResolver {

	@Override
	public Permission resolvePermission(String permissionString) {
		
		if (permissionString.startsWith("+")) {
			return new BitPermission(permissionString);
		}
		
		return new WildcardPermission(permissionString);
	}

}
