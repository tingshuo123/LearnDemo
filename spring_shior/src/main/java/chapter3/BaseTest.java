package chapter3;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.apache.shiro.util.ThreadContext;
import org.junit.After;

public class BaseTest {

	@After
	public void tearDown() {
		ThreadContext.unbindSubject();
	}

	protected void loing(String configFile, String username, String password) {
		// 获取SecurityManager工厂
		Factory<org.apache.shiro.mgt.SecurityManager> factory = new IniSecurityManagerFactory(configFile);

		// 获通过工厂获取SecurityManager实例
		SecurityManager securityManager = factory.getInstance();

		// 将实例绑定给SecurityUtils
		SecurityUtils.setSecurityManager(securityManager);

		// 获取Subject
		Subject subject = SecurityUtils.getSubject();

		// 创建Token
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);

		// 登录（验证用户）
		try {
			subject.login(token);
		} catch (AuthenticationException e) {
			// 验证失败
			e.printStackTrace();
		}

		if (subject.isAuthenticated()) {
			System.out.println("登录成功！！！");
		}
	}
	
	public Subject subject() {
		return SecurityUtils.getSubject();
	}
}
