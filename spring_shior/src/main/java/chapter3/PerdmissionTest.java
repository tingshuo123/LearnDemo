package chapter3;

import org.junit.Assert;
import org.junit.Test;

public class PerdmissionTest extends BaseTest {
	
	@Test
	public void testIsPermitted() {
		
		this.loing("classpath:shiro-primission.ini", "zhang", "123");
		// 判断是否拥有单个权限
		Assert.assertTrue(this.subject().isPermitted("user:create"));
		// 判断是否拥有多个个权限
		Assert.assertTrue(this.subject().isPermittedAll("user:create", "user:update"));
	}
}
