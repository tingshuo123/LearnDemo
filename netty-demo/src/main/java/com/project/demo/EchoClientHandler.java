package com.project.demo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

// 客户端消息处理器
@Sharable
public class EchoClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
    
    // 接收数据时被调用
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        System.out.println("Chient received: " + msg.toString(CharsetUtil.UTF_8));
    }
    
    // 建立时被调用
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
       // 发送消息到服务器
       ctx.writeAndFlush(Unpooled.copiedBuffer("Hello！", CharsetUtil.UTF_8));
    }
    
    // 处理过程中发生异常时被调用
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
       cause.printStackTrace();
       ctx.close();
    }
}
