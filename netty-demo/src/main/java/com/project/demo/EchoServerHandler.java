package com.project.demo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

// 服务消息处理器
@Sharable // 标识 Channel-Handler 可以被多个 Channel 安全共享
public class EchoServerHandler extends ChannelInboundHandlerAdapter {
    
    // 处理服务器接收到的消息
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        // 读取消息
        ByteBuf in = (ByteBuf) msg;
        System.out.println("Server received: " + in.toString(CharsetUtil.UTF_8));
        
        // 将消息写给发送者
        ctx.write(Unpooled.copiedBuffer("你好！", CharsetUtil.UTF_8));
    }
    
    // 批量消息中的最后一条消息条被调用
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        // 将消息冲刷到远程节点，并关闭该 Channel
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER)
            .addListener(ChannelFutureListener.CLOSE);
    }
    
    // 在读取消息期间，发生异常时调用
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        
        cause.printStackTrace();
        ctx.close();
    }
}
