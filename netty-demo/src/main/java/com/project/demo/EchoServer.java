package com.project.demo;

import java.net.InetSocketAddress;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;


/**
 * 1. 创建一个ServerBootstrap的实例以引导和绑定服务器
 * 2. 创建并分配一个NioEventLoopGroup实例以进行事件的处理
 * 3. 指定服务器绑定的本地的InetSocketAddress
 * @author wangqingjie
 *
 * 2019年1月3日-下午4:23:55
 */
public class EchoServer {

    private int port;
    
    public EchoServer(int port) {
        this.port = port;
    }

    public void start() throws Exception {
        // 消息处理器实例
        final EchoServerHandler serverHandler = new EchoServerHandler();
        
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            // 配置
            b.group(group)
                .channel(NioServerSocketChannel.class)  // 指定使用的 NIO 
                .localAddress(new InetSocketAddress(port))  // 指定端口
                .childHandler(new ChannelInitializer() {  // 指定自定义的 Handle
                    @Override
                    protected void initChannel(Channel ch) throws Exception {
                        ch.pipeline().addLast(serverHandler);
                    }
                });
            // 绑定服务器
            ChannelFuture f = b.bind().sync();
            // 获取 channel 的 CloseFuture
            f.channel().closeFuture().sync();
        } finally {
            // 关闭EventLoopGroup，释放所有的资源
            group.shutdownGracefully().sync();
        }
    }

    public static void main(String[] args) throws Exception {
        new EchoServer(8081).start();  // 启动
    }
}
