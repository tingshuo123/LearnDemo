package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * mybatisPlus 配置类
 * @author wqj24
 *
 */
@Configuration
// 项目启动自动加载配置
@ImportResource(locations = {"classpath:/mybatis/spring-mybatis.xml"})
public class MybatisPulsConfig {
	
}
