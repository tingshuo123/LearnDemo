package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 配置 Swagger2
 *
 */
@EnableSwagger2
@Configuration
public class SwaggerConfig {
	
	// 是否开启 swagger，正式环境一般关闭，可以通过SpringBoot多环境配置
	@Value(value = "${swagger.enabled}")
	private Boolean swaggerEnabled;
	
	@Bean
	public Docket createRestApi() {
		
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(this.apiInfo())
				// 是否开启
				.enable(swaggerEnabled).select()
				// 扫描的包路径
				.apis(RequestHandlerSelectors.basePackage("com.example.demo"))
				// 指定路径处理PathSelectors.any()代表所有路径
				.paths(PathSelectors.any()).build().pathMapping("/");
	}

	/**
	 * 设置api信息
	 * @return
	 */
	private ApiInfo apiInfo() {
		
		return new ApiInfoBuilder()
				.title("SpringBoot-Swagger2集成和使用-demo展示")
				.description("TingShuo")
				// 作者信息
				.contact(new Contact("TingShuo", "https://www.jianshu.com/u/945f89e93afe\n" + 
						"", "2413146882@qq.com"))
				.version("1.0.0")
				.build();
	}
}
