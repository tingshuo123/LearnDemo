package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * 生成器，根据表结构，自动生成 IDao，Mapper.xml, 实体，service
 * @author wqj24
 *
 */
public class MysqlGenerator {
	
	/**
	 * JDBC相关配置
	 */
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://127.0.0.1:3306/test?useSSL=false&useUnicode=true&characterEncoding=UTF-8";
	private static final String USER_NAME = "root";
	private static final String PASSWORD = "";
	
	public static void main(String[] args) {
		
		String projectPath = System.getProperty("user.dir");  // 获取项目路径
		String outPut = projectPath + "/src/main/java";
		
		/*
		 * 设置自动填充字段
		 * 
		 * 比如每张表中都有创建时间跟修改时间两个字段
		 * 新增时，创建时间跟修改时间同时修改。
		 * 更新时，修改修改时间。
		 * 
		 * 可以使用公共字段填充功能，实现自动按场景更新
		 */
		List<TableFill> tableFillList = new ArrayList<TableFill>();
		TableFill createField = new TableFill("gmt_create", FieldFill.INSERT);
		TableFill modifiedField = new TableFill("gmt_modified", FieldFill.INSERT_UPDATE);
		tableFillList.add(createField);
		tableFillList.add(modifiedField);
		
		
		/**
		 * 全局配置
		 */
		GlobalConfig globalConfig = new GlobalConfig()
				.setOutputDir(outPut)  // 生成文件输出目录
				.setFileOverride(true)  // 是否覆盖
				.setActiveRecord(false)  // 是否开启 ActiveRecored模式
				.setAuthor("wangqingjie")  // 作者
				.setXmlName("%sMapper")  // mppper.xml 名
				.setMapperName("%sDao");  // Mapper 接口名
				
		/**
		 * 数据源配置
		 */
		DataSourceConfig dataSourceConfig = new DataSourceConfig()
				// 自定义类型转换
//				.setTypeConvert(new MySqlTypeConvert())
				.setDbType(DbType.MYSQL)  // 设置数据库类型
				.setDriverName(DRIVER)  // mysql 驱动
				.setUrl(URL)
				.setUsername(USER_NAME)
				.setPassword(PASSWORD);
				
		/**
		 * 生成策略
		 */
		StrategyConfig strategyConfig = new StrategyConfig()
				.setInclude("test")  // 需要自动生成的表
				.setDbColumnUnderline(true)  //表名、字段名、是否使用下划线命名（默认 false）
				.setNaming(NamingStrategy.underline_to_camel)  // 下划线转驼峰
				.setTableFillList(tableFillList)  // 自动填充字段
				.setEntityColumnConstant(true)  // 生成常量字段
				.setEntityBuilderModel(true)  // 构建者模式
				.setEntityLombokModel(true); // 实体  lombok 模型
		
		/**
		 * 包配置
		 */
		PackageConfig packageConfig = new PackageConfig()
				.setParent("com.example.demo")  // 父包
				.setController("controller")
				.setXml("mapper")  // mapper.xml 文件包
				.setMapper("dao");  // 接口包
		
		TemplateConfig templateConifg = new TemplateConfig()
				.setXml(null);  // xml 文件生成至根目录
				
		/**
		 * 配置代码生成器
		 */
		AutoGenerator mpg = new AutoGenerator()
				.setGlobalConfig(globalConfig)
				.setDataSource(dataSourceConfig)
				.setStrategy(strategyConfig)
				.setPackageInfo(packageConfig)
				.setTemplate(templateConifg);
		
		// 执行生成
		mpg.execute();
	}
}
