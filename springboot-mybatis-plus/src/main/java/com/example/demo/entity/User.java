package com.example.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangqingjie
 * @since 2018-10-22
 */
@Data
@Accessors(chain = true)
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5829478463898471097L;
	/**
	 * 唯一标示
	 */
	private Long id;
	/**
	 * 编码
	 */
	private String code;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 状态 1启用 0 停用
	 */
	private StatusEnum status;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date gmtModified;

	public static final String ID = "id";

	public static final String CODE = "code";

	public static final String NAME = "name";

	public static final String STATUS = "status";

	public static final String GMT_CREATE = "gmt_create";

	public static final String GMT_MODIFIED = "gmt_modified";

}
