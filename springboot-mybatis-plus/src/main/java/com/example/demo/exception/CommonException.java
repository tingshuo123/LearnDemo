package com.example.demo.exception;

/**
 * 自定义异常
 *
 */
public class CommonException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7494955029688688363L;

	/**
	 * 异常代码
	 */
	private String code;
	
	/**
	 * 异常提示
	 */
	private String msg;

	public CommonException(String code, String msg) {
		super(code + msg);
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
