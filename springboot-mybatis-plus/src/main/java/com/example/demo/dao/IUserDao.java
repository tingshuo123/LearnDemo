package com.example.demo.dao;

import com.example.demo.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangqingjie
 * @since 2018-10-22
 */
public interface IUserDao extends BaseMapper<User> {
	
	public void updateAutoIncrement(Long num);
}
