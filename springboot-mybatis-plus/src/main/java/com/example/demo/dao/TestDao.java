package com.example.demo.dao;

import com.example.demo.entity.Test;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangqingjie
 * @since 2018-11-30
 */
public interface TestDao extends BaseMapper<Test> {

}
