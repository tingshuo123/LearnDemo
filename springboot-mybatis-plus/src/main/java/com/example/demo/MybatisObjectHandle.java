package com.example.demo;

import java.util.Date;

import org.apache.ibatis.reflection.MetaObject;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;

/**
 * 自动填充字段
 * @author wqj24
 *
 */
public class MybatisObjectHandle extends MetaObjectHandler{
	
	/**
	 * 插入
	 */
	@Override
	public void insertFill(MetaObject metaObject) {
		// 需要自动填充的字段
		setFieldValByName("gmtCreate", new Date(), metaObject);
		setFieldValByName("gmtModified", new Date(), metaObject);
		
	}
	
	/**
	 * 更新
	 */
	@Override
	public void updateFill(MetaObject metaObject) {
		// 需要自动填充的字段
		setFieldValByName("gmtModified", new Date(), metaObject);
	}
	
	
}
