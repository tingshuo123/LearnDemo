package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.example.demo.entity.Test;
import com.example.demo.service.ITestService;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangqingjie
 * @since 2018-11-30
 */
@RestController
@RequestMapping("/user")
public class TestController {

    @Autowired
    ITestService service;

    /**
     * 通过id查询
     * 
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Test getUser(@PathVariable("id") int id) {

        return service.selectById(id);
    }

    /**
     * 获取所有用户
     * 
     * @return
     */
    @GetMapping
    public List<Test> getAll() {

        return service.selectList(new EntityWrapper<Test>());
    }

    /**
     * 添加用户
     * 
     * @param user
     * @return
     */
    @PostMapping
    public boolean AddUser(Test user) {

        return service.insert(user);
    }

    /**
     * 删除指定 id 用户
     * 
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public boolean Delete(@PathVariable("id") int id) {

        return service.deleteById(id);
    }
}
