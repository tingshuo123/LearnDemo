package com.example.demo.controller;

import com.example.demo.entity.StatusEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//加入@ApiModel
@ApiModel
public class UserResp {
	
	@ApiModelProperty(value="ID", dataType="String", name="ID", example="1053194693111738369")
	private String id;
	@ApiModelProperty(value="编码", dataType="String", name="code", example="001")
	private String code;
	@ApiModelProperty(value="用户名", dataType="String", name="name", example="任平生")
	private String name;
	@ApiModelProperty(value="账户状态", dataType="String", name="name", example="ENABLE")
	private StatusEnum status;
}
