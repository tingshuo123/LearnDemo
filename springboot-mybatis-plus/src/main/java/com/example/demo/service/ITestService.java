package com.example.demo.service;

import com.example.demo.entity.Test;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangqingjie
 * @since 2018-11-30
 */
public interface ITestService extends IService<Test> {

}
