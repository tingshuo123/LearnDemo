package com.example.demo.service;

import com.example.demo.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangqingjie
 * @since 2018-10-22
 */
public interface IUserService extends IService<User> {

}
