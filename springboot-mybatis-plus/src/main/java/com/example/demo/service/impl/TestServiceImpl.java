package com.example.demo.service.impl;

import com.example.demo.entity.Test;
import com.example.demo.dao.TestDao;
import com.example.demo.service.ITestService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangqingjie
 * @since 2018-11-30
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestDao, Test> implements ITestService {

}
