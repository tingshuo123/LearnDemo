package com.example.demo.service.impl;

import com.example.demo.entity.User;
import com.example.demo.dao.IUserDao;
import com.example.demo.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangqingjie
 * @since 2018-10-22
 */
@Service
public class UserServiceImpl extends ServiceImpl<IUserDao, User> implements IUserService {

}
