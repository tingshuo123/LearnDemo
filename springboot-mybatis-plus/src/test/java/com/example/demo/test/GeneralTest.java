package com.example.demo.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dao.IUserDao;
import com.example.demo.entity.User;
import com.example.demo.service.IUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * CRUD 测试
 * @author wqj24
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class GeneralTest {
	
	@Autowired
	IUserService userService;
	@Autowired
	IUserDao iUserDao;
	/**
	 * 插入
	 */
	@Test
	public void insertTest() {
		
		User user = new User();
		user.setId(Long.valueOf(11));
		user.setCode("002");
		user.setName("忽如远行");
		try {
			userService.insert(user);
		} catch (DuplicateKeyException e) {
			userService.updateById(user);
		}
		userService.insert(user);
		
		log.info("添加结束");
	}
	
	/**
	 * 更新
	 */
	@Test
	public void updateTest() {
		
		User user = new User();
		user.setName("任平生");
		user.setId(Long.valueOf("1054283161107816450"));
		userService.updateById(user);
		
		log.info("更新结束");
	}
	
	/**
	 * 删除
	 */
	@Test
	public void deleteTest() {
		User user = new User();
		user.setId(Long.valueOf("1054285921517584385"));
		user.setCode("001");
		
		// 既可以传id，也可以传实体对象
//		userService.deleteById(user);
		userService.deleteById(user.getId());
		log.info("删除结束");
	}
	
	/**
	 * 查询
	 */
	@Test
	public void SelectTest() {
		User user = new User();
		user.setId(Long.valueOf("1054282905603309570"));
		userService.selectById(user);
		log.info("查询：{}", userService.selectById(user.getId()));
	}
	
	/**
	 * 设置自增值
	 */
	@Test
	public void updateAutoIncrement() {
		iUserDao.updateAutoIncrement(Long.parseLong("1010"));
	}
}
