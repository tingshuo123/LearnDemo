package com.example.demo.exception;

public class CommonException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7494955029688688363L;

	String code;
	String msg;

	public CommonException(String code, String msg) {
		super(code + msg);
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
