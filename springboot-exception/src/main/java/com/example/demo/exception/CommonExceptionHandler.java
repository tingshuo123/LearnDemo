package com.example.demo.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

/**
 * 同一异常处理类
 * @author wqj24
 *
 */
@ControllerAdvice
@Slf4j
public class CommonExceptionHandler {
	
	@ExceptionHandler(Exception.class)  // 指定需要拦截的异常类
	@ResponseBody
	public Map<String, Object> exceptionHandler(Exception e) {
		
		log.info("CommonException：{}({})", e);
		
		Map<String, Object> result = new HashMap<>();
		result.put("respCode", "1010");
		result.put("respMsg", e.getMessage());
		
		
		return result;
		
	}
	
	@ExceptionHandler(CommonException.class)
	@ResponseBody
	public Map<String, Object> exceptionHandler(CommonException e) {
		
		log.info("CommonException：{}({})",e.getMsg(), e.getCode());
		
		Map<String, Object> result = new HashMap<>();
		result.put("respCode", e.getCode());
		result.put("respMsg", e.getMsg());
		
		return result;
	}
	
	@ExceptionHandler(BindException.class)
	@ResponseBody
    public Map<String,Object> handleBindException(BindException ex) {
        //校验 除了 requestbody 注解方式的参数校验 对应的 bindingresult 为 BeanPropertyBindingResult
        FieldError fieldError = ex.getBindingResult().getFieldError();
        log.info("必填校验异常:{}({})", fieldError.getDefaultMessage(),fieldError.getField());
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("respCode", "01002");
        result.put("respMsg", fieldError.getDefaultMessage());
        return result;
    }
}
