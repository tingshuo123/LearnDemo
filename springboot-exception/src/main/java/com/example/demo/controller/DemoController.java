package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.bean.DemoReq;
import com.example.demo.exception.CommonException;

@RestController
public class DemoController {
	
	@GetMapping("/demo")
	public String demo() {
		
		throw new CommonException("01001", "发送异常");
	}
	
	@GetMapping("/demo/valid")
	public String demoValid(@Valid DemoReq req) {
		
		return req.getCode() + ", " + req.getName();
	}
}
