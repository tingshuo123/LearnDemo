package com.example.demo.bean;



import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DemoReq {
	
	@NotBlank(message = "code不能为空")
	String code;
	@Length(max=5, message = "长度不能超过5")
	String name;
}
