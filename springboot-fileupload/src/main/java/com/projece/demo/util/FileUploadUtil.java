package com.projece.demo.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

public class FileUploadUtil {
	
	/**
	 * 多文件保存
	 * @param req HttpServletRequest
	 * @return
	 */
	public static List<String> saveFiles(HttpServletRequest req) {

		List<String> fileNameList = new ArrayList<>();

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(req.getServletContext());

		// 检查 form 是否有 enctype=“multipart/form-data” 属性值
		if (multipartResolver.isMultipart(req)) {

			// 转化为多部分的 request
			MultipartHttpServletRequest multipartRequeset = (MultipartHttpServletRequest) req;

			// 获取 multiRequest 中所有文件
			Iterator<String> iter = multipartRequeset.getFileNames();

			// 遍历
			while (iter.hasNext()) {

				// 获取文件
				MultipartFile file = multipartRequeset.getFile(iter.next().toString());

				// 检查文件是否存在
				if (file != null && !file.isEmpty()) {
					// 保存文件
					String filename = saveFile(req, file);
					fileNameList.add(filename);
				}
			}
		}

		return fileNameList;
	}
	
	/**
	 * 单文件保存
	 * @param req HttpServletRequest
	 * @param file MultipartFile
	 * @return
	 */
	public static String saveFile(HttpServletRequest req, MultipartFile file) {

		String result = null;

		// 获取原始文件名
		String origFileName = file.getOriginalFilename();

		// 文件新名字
		String newFileName = UUID.randomUUID() + "_" + origFileName;

		// 设置文件保存路径
		ServletContext context = req.getServletContext();
		String path = context.getRealPath("/upload");

		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		File newFile = new File(path, newFileName);

		try {
			file.transferTo(newFile);
			result = newFileName;
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}

		return result;
	}
}
