package com.projece.demo.controller;

import java.io.File;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.projece.demo.util.FileUploadUtil;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class FileUploadController {
	
	@PostMapping("/upload")
	public String upload(@RequestParam("file") MultipartFile file, HttpServletRequest req) {
		
		if (file.isEmpty()) {
			return "上传文件不能为空";
		}
		
		String fileName = FileUploadUtil.saveFile(req, file);
		
		return "文件 ' " + fileName + "' 上传成功";
	}
}
