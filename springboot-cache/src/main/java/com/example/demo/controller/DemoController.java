package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Person;
import com.example.demo.service.IPersonService;

@RestController("/person")
public class DemoController {
	
	@Autowired
	IPersonService personService;
	
	/**
	 * 添加用户
	 * @param person
	 * @return
	 */
	@PostMapping
	public Person save(Person person) {
		
		return personService.save(person);
	}
	
	/**
	 * 查找用户
	 * @param person
	 * @return
	 */
	@GetMapping
	public Person getOne(Person person) {
		
		return personService.findOne(person);
	}
	
	/**
	 * 删除用户
	 * @param id
	 * @return
	 */
	@DeleteMapping
	public String delete(Long id) {
		personService.remove(id);
		return "ok";
	}
	
}
