package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity   // jpa 实体类注解
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"}) // 解决 No serializer 异常
public class Person {
	
	@Id
	private Long id;
	
	private String name;
	
	private Integer age;
	
	private String address;
}
