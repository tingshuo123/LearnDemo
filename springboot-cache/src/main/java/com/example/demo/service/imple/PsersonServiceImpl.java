package com.example.demo.service.imple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Person;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.IPersonService;

@Service
public class PsersonServiceImpl implements IPersonService{
	
	@Autowired
	PersonRepository personRepository;
	
	@Override
	@CachePut(value = "peopele", key = "#person.id")  // 将数据添加到缓存中，id 作为 key
	public Person save(Person person) {
		
		return personRepository.save(person);
	}

	@Override
	@CacheEvict(value = "peopele") // 删除 key 为id 的缓存，（没有指定key，会将参数作为key）
	public void remove(Long id) {
		
		personRepository.deleteById(id);
	}

	@Override
	@Cacheable(value = "peopele", key = "#person.id")  // 先查询缓存，有则直接返回缓存里的数据，没有就将返回值存到缓存中
	public Person findOne(Person person) {
		
		return personRepository.getOne(person.getId());
	}
	
	
}
