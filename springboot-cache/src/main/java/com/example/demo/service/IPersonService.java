package com.example.demo.service;

import com.example.demo.entity.Person;

public interface IPersonService {
	
	/**
	 * 保存个人信息
	 * @param person
	 * @return
	 */
	public Person save(Person person);
	
	/**
	 * 通过 id 删除个人信息
	 * @param id
	 */
	public void remove(Long id);
	
	/**
	 * 查找一个个人信息
	 * @param person
	 * @return
	 */
	public Person findOne(Person person);
}
