package com.example.demo;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTempTest {
	
	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	
	@Test
	public void test() {
		redisTemplate.opsForValue().set("test-key", new Date());
		System.out.println(redisTemplate.opsForValue().get("test-key"));
		
	}
	
}
