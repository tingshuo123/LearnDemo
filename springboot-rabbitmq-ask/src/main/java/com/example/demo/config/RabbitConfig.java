package com.example.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    
    @Bean
    public Queue QueueA() {
        
        return new Queue("hello");
    }
    
    @Bean
    public Queue QueueB() {
        
        return new Queue("helloObj");
    }
    
    /**
     * fanout 类型交换机，广播模式，发送消息给 fanout 交换机，fanout 交换机会把消息转发给跟它绑定了的队列
     */
    @Bean
    FanoutExchange fanoutExchange() {
        
        return new FanoutExchange("ABExchange");
    }
    
    /**
     * 绑定
     */
    @Bean
    Binding bindingExchangeA(Queue QueueA, FanoutExchange fanoutExchange) {
        
        return BindingBuilder.bind(QueueA).to(fanoutExchange);
    }
    
    @Bean
    Binding bindingExchangeB(Queue QueueB, FanoutExchange fanoutExchange) {
        
        return BindingBuilder.bind(QueueB).to(fanoutExchange);
    }
}
