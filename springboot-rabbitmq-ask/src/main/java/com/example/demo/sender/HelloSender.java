package com.example.demo.sender;

import java.util.Date;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class HelloSender {
    
    @Autowired
    private RabbitTemplate rabbitTemplate;
    
    public void send() {
        String msg = "现在时间：" + new Date();
        System.out.println("HelloSender 发送:" + msg);
        
        // 设置消息是否到达 Exchange 的回调， ack 为 true 到达， false 没有到达
        this.rabbitTemplate.setConfirmCallback((correlationData, ask, cause) -> {
            System.out.println("correlationData: " + correlationData);
            if (ask) {
               System.out.println("消息发送成功");
            } else {
               System.out.println("消息发送失败，原因： " + cause);
            }
        });
        
        // 如果消息没有到达 Queue 回调此方法
        this.rabbitTemplate.setReturnCallback((Message message, int replyCode, String replyText,
                String exchange, String routingKey)-> {
            System.out.println("路由失败：" + message);
            System.out.println(String.format("message: %s, replyCode: %3d, replyText: %s, exchange: %s routing_key: %s", message, replyCode, replyCode, replyText, exchange, routingKey));
        });
        
        // 发送消息
        this.rabbitTemplate.convertAndSend("ABExchange", "123" , msg);
    }
}
