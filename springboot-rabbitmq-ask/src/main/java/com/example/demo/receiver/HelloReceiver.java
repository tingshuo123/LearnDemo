package com.example.demo.receiver;

import java.io.IOException;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

@Component
@RabbitListener(queues = "hello")
public class HelloReceiver {

    @RabbitHandler
    public void process(String hello, Channel channel, Message message) throws IOException {
        try {
            System.out.println("HelloReceiver1 收到：" + hello); // 处理消息
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false); // 确认
        } catch (Exception e) {
            e.printStackTrace();  // 业务处理
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), true); // 将消息重新放入队列
        }
    }
}