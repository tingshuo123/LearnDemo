package com.example.demo.controller;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class LogController {
    
    @GetMapping("/log")
    public String demo() {
        log.info("get /log");
        printLog();
        return "log";
    }
    
    private void printLog() {
        int count = 60 * 60 * 24;
        for (int i=0; i<count; i++) {
            log.info("LogController log: " + i);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
