package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling  // 定时任务注解
@SpringBootApplication
public class SpringbootTimedTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootTimedTaskApplication.class, args);
	}
}
